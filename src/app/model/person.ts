export class Person {
  id: number;
  firstName: string;
  lastName: string;
  meeting_place: string;
}
