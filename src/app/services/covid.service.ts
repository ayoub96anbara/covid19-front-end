import { Injectable } from '@angular/core';
import {AbstractService} from './abstract.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CovidService {

  constructor(private abstractService:AbstractService<any>) { }

  getData(url:string):Observable<any>{
    return   this.abstractService.callGet(url);
  }


}
