import { Injectable } from '@angular/core';
import {AbstractService} from './abstract.service';
import {Observable} from 'rxjs';
import {Person} from '../model/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private abstractService:AbstractService<any>) { }

  getAllPersons(url:string):Observable<any>{
    return   this.abstractService.callGet(url);
  }
  addPerson(url:string,person:Person):Observable<any>{
    return   this.abstractService.callPost(url,person);
  }
}

