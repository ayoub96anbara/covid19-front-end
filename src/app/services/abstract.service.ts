import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AbstractService<T> {

  constructor(private http: HttpClient) { }

  callGet(url:string):Observable<T>{
    return  this.http.get<T>(url);
  }
  callPost(url:string,object:any):Observable<T>{
    return  this.http.post<T>(url,object);
  }
}
