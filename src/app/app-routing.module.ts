import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SuivieComponent} from './components/suivie/suivie.component';
import {TracageComponent} from './components/tracage/tracage.component';
import {ContactComponent} from './components/contact/contact.component';
import {NotFoundComponent} from './components/not-found/not-found.component';


const routes: Routes = [
  {path:'suivie', component: SuivieComponent},
  {path:'tracage', component: TracageComponent},
  {path:'contact', component: ContactComponent},
  { path: '',   redirectTo: 'suivie', pathMatch: 'full' }, // redirect to SuivieComponent
{ path: '**', component: NotFoundComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
