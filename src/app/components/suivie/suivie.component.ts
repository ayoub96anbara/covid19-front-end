import {Component, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import {CovidService} from '../../services/covid.service';
import {API_Covid} from '../../config/api.url.config';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-suivie',
  templateUrl: './suivie.component.html',
  styleUrls: ['./suivie.component.css']
})
export class SuivieComponent implements OnInit {
  myHighcharts = Highcharts;
  showHighcharts: boolean = false;


  optionsChart = {
    colors: [ '#8bbc21'],

    title: {
      text: 'Développement de Covid-19 au Maroc'
    },
    chart: {
      type: 'spline'
    },
    subtitle: {
      text: 'Source: ' + API_Covid.api_covid
    },
    yAxis: {
      title: {
        text: 'cas confirmés'
      }
    },

    xAxis: {
      categories: []
    },

    series: [{
      name: 'cases',
      data: []
    }],
  }
  ;

  constructor(private covidService: CovidService,private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.spinner.show();
    this.getData();
  }

  getData() {
    this.covidService.getData(API_Covid.api_covid)
      .subscribe(
        (data) => {
          console.log(data);
          this.optionsChart.xAxis.categories = data.dateList;
          this.optionsChart.series[0].data = data.casesList;

        }
        , error => {
          console.error(error);
        },
        () => {
          this.showHighcharts = true;
          this.spinner.hide();
          // console.log(this.optionsChart.xAxis.categories);
          // console.log( this.optionsChart.series[0].data);
        }
      );
  }
}
