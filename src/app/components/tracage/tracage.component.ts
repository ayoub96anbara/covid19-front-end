import { Component, OnInit } from '@angular/core';
import {Person} from '../../model/person';
import {PersonService} from '../../services/person.service';
import {API_URL_Person} from '../../config/api.url.config';

@Component({
  selector: 'app-tracage',
  templateUrl: './tracage.component.html',
  styleUrls: ['./tracage.component.css']
})
export class TracageComponent implements OnInit {

  persons:Person[]=[];

  constructor(private personService: PersonService) {
  }
  ngOnInit(): void {
    this.onDataChange();
  }
  onDataChange() {
    this.personService.getAllPersons(API_URL_Person.api_persons)
      .subscribe(
        (data) => {
          // TableUsersComponent.users=data;
          this.persons = data._embedded.persons;
          console.log(data);
        }, err => {
          console.log(err);
        }
      );
  }

}
