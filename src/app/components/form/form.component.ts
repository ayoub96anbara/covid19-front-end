import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonService} from '../../services/person.service';
import {Person} from '../../model/person';
import {API_URL_Person} from '../../config/api.url.config';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Output('onDataChange') onDataChange=new EventEmitter();

  personFormGroup=new FormGroup(
    {
      firstNameControl:new FormControl('',Validators.required),
      lastNameControl:new FormControl('',
        [Validators.required,Validators.minLength(3),Validators.maxLength(40)]),
      addressControl:new FormControl('',Validators.maxLength(100))
    }
  );



  constructor(private personService:PersonService) { }

  ngOnInit(): void {
  }

  addPerson() {
    let person=new Person();
    person.firstName=this.personFormGroup.controls.firstNameControl.value;
    person.lastName=this.personFormGroup.controls.lastNameControl.value;
    person.meeting_place=this.personFormGroup.controls.addressControl.value;
    this.personService.addPerson(API_URL_Person.api_persons,person)
      .subscribe(
        (data)=>{
      this.onDataChange.emit();
          //console.log(data);
          //TableUsersComponent.users.push(data)
        },err => {
          console.log(err);
        }
      );


  }
}
